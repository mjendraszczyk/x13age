<?php
/**
 * Main class of module mjautocloseproduct
 * @author x13.pl.
 * @copyright (c) 2019, x13.pl
 * @license http://x13.pl x13.pl
 */

class X13age extends Module
{

    public function __construct()
    {
        $this->name = 'x13age';
        $this->version = '1.0.0';
        $this->tab = 'administration';

        $this->author = 'x13.pl';
        $this->bootstrap = true;

        parent::__construct();
        $this->ps_version = Tools::substr(_PS_VERSION_, 0, 3);

        $this->displayName = $this->l('X13 Age confirmation');
        $this->description = $this->l('Display confirmation on homepage are have 18 old year');
    }

    public function install()
    {

        $languagesSettings = array(
            "pl" => array(
                'x13age_title_l' => "Potwierdź swój wiek",
                'x13age_message_l' => "Potwierdź swój wiek aby upewnić się, że jesteś pełnoletni!",
                'x13age_wrong_information_l' => "Nie jesteś pełnoletni!",
                'x13age_btn_confirmation_l' => "Potwierdzam",
                'x13age_btn_canceled_l' => "Anuluj",
            ),
            "en" => array(
                'x13age_title_l' => "Confirm your's age",
                'x13age_message_l' => "Confirm your age to make sure you are 18!",
                'x13age_wrong_information_l' => "You are not of legal age!",
                'x13age_btn_confirmation_l' => "Confirm",
                'x13age_btn_canceled_l' => "Cancel",
            )
        );
        $languages = Language::getLanguages(false);
        foreach ($languages as $lang) {
            if ($lang['iso_code'] == 'pl') {
                Configuration::updateValue('x13age_title_l' . $lang['id_lang'], $languagesSettings['pl']['x13age_title_l']);
                Configuration::updateValue('x13age_message_l' . $lang['id_lang'], $languagesSettings['pl']['x13age_message_l']);
                Configuration::updateValue('x13age_wrong_information_l' . $lang['id_lang'], $languagesSettings['pl']['x13age_wrong_information_l']);


                Configuration::updateValue('x13age_btn_confirmation_l' . $lang['id_lang'], $languagesSettings['pl']['x13age_btn_confirmation_l']);
                Configuration::updateValue('x13age_btn_canceled_l' . $lang['id_lang'], $languagesSettings['pl']['x13age_btn_canceled_l']);
            } else {
                Configuration::updateValue('x13age_title_l' . $lang['id_lang'], $languagesSettings['en']['x13age_title_l']);
                Configuration::updateValue('x13age_message_l' . $lang['id_lang'], $languagesSettings['en']['x13age_message_l']);
                Configuration::updateValue('x13age_wrong_information_l' . $lang['id_lang'], $languagesSettings['en']['x13age_wrong_information_l']);


                Configuration::updateValue('x13age_btn_confirmation_l' . $lang['id_lang'], $languagesSettings['en']['x13age_btn_confirmation_l']);
                Configuration::updateValue('x13age_btn_canceled_l' . $lang['id_lang'], $languagesSettings['en']['x13age_btn_canceled_l']);
            }
        }

        Configuration::updateValue('x13age_link', 'https://google.com');

        Configuration::updateValue('x13age_width', '65%');
        Configuration::updateValue('x13age_height', 'auto');
        Configuration::updateValue('x13age_round', '0');

        Configuration::updateValue('x13age_color_frame', '#505050');
        Configuration::updateValue('x13age_color_btn_confirm', '#11aa11');
        Configuration::updateValue('x13age_color_font_btn_confirm', '#ffffff');
        Configuration::updateValue('x13age_color_btn_danger', '#aa1111');
        Configuration::updateValue('x13age_color_font_btn_danger', '#ffffff');

        return parent::install() && $this->registerHook('displayFooter') && $this->registerHook('displayHeader');
    }

    public function hookdisplayHeader($params)
    {
        if ($this->ps_version >= '1.7') {
            $this->context->controller->registerStylesheet('modules-' . $this->name, 'modules/' . $this->name . '/views/css/' . $this->name . '.css', array('media' => 'all', 'priority' => 150));
            $this->context->controller->registerJavascript('modules-' . $this->name, 'modules/' . $this->name . '/views/js/' . $this->name . '.js', array('position' => 'bottom', 'priority' => 150));
        } else {
            if ($this->ps_version == '1.5') {
                $this->context->controller->addJS($this->_path . 'views/js/x13ageBootstrap.js');
            }
            $this->context->controller->addJS($this->_path . 'views/js/' . $this->name . '.js');
            $this->context->controller->addCSS($this->_path . 'views/css/' . $this->name . '.css');
        }
    }

    public function uninstall()
    {
        return parent::uninstall();
    }

    public function renderForm()
    {
        $fields_form = array();
        if ($this->ps_version >= '1.6') {
            $root = Category::getRootCategory();
            $tree = new HelperTreeCategories('x13age_categories_tree');
            $tree->setUseCheckBox(true)
                    ->setAttribute('is_category_filter', $root->id)
                    ->setRootCategory($root->id)
                    ->setFullTree(true)
                    ->setSelectedCategories((array) (unserialize(Configuration::get('x13age_categories_tree_filled'))))
                    ->setInputName('x13age_categories_tree');
            $x13age_categories_tree = $tree->render();

            $fields_form[0]['form'] = array(
                'legend' => array(
                    'title' => $this->l('Settings'),
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Display choose date'),
                        'size' => '5',
                        'name' => 'x13age_date',
                        'is_bool' => true,
                        'required' => false,
                        'values' => array(
                            array(
                                'id' => 'x13age_date_yes',
                                'value' => 1,
                                'label' => $this->l('Yes')
                            ),
                            array(
                                'id' => 'x13age_date_no',
                                'value' => 0,
                                'label' => $this->l('No')
                            )
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Display choose category'),
                        'size' => '5',
                        'name' => 'x13age_category',
                        'is_bool' => true,
                        'required' => false,
                        'values' => array(
                            array(
                                'id' => 'x13age_category_yes',
                                'value' => 1,
                                'label' => $this->l('Yes')
                            ),
                            array(
                                'id' => 'x13age_category_no',
                                'value' => 0,
                                'label' => $this->l('No')
                            )
                        ),
                    ),
                    array(
                        'type' => 'categories_select',
                        'label' => $this->l('Choose categories'),
                        'name' => 'x13age_categories_tree',
                        'category_tree' => $x13age_categories_tree
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                    'class' => 'btn btn-default pull-right',
                ),
            );
        } else {
            $categories = (new Category())->getSimpleCategories(Configuration::get('PS_LANG_DEFAULT'));

            $category_list = array();
            foreach ($categories as $key => $c) {
                $category_list[$key] = array(
                    'id_category' => $c['id_category'],
                    'name' => $c['name'],
                );
            }

            $fields_form[0]['form'] = array(
                'legend' => array(
                    'title' => $this->l('Settings'),
                ),
                'input' => array(
                    array(
                        'type' => 'radio',
                        'label' => $this->l('Display choose date'),
                        'size' => '5',
                        'name' => 'x13age_date',
                        'is_bool' => true,
                        'required' => false,
                        'values' => array(
                            array(
                                'id' => 'x13age_date_yes',
                                'value' => 1,
                                'label' => $this->l('Yes')
                            ),
                            array(
                                'id' => 'x13age_date_no',
                                'value' => 0,
                                'label' => $this->l('No')
                            )
                        ),
                    ),
                    array(
                        'type' => 'radio',
                        'label' => $this->l('Display choose category'),
                        'size' => '5',
                        'name' => 'x13age_category',
                        'is_bool' => true,
                        'required' => false,
                        'values' => array(
                            array(
                                'id' => 'x13age_category_yes',
                                'value' => 1,
                                'label' => $this->l('Yes')
                            ),
                            array(
                                'id' => 'x13age_category_no',
                                'value' => 0,
                                'label' => $this->l('No')
                            )
                        ),
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Choose categories'),
                        'name' => 'x13age_categories_tree[]',
                        'multiple' => true,
                        'cols' => 5,
                        'class' => 'form-control',
                        'options' => array(
                            'query' => $category_list,
                            'id' => 'id_category',
                            'name' => 'name',
                        )
                    )
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                    'class' => 'btn btn-default pull-right',
                ),
            );
        }

        $fields_form[1]['form'] = array(
            'legend' => array(
                'title' => $this->l('Content'),
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('Title'),
                    'size' => '5',
                    'name' => 'x13age_title',
                    'required' => false,
                    'lang' => true
                ),
                array(
                    'type' => 'textarea',
                    'label' => $this->l('Description'),
                    'desc' => $this->l('Yours description'),
                    'size' => '10',
                    'class' => 'rte',
                    'autoload_rte' => true,
                    'cols' => 60,
                    'name' => 'x13age_message',
                    'lang' => true
                ),
                array(
                    'type' => 'textarea',
                    'label' => $this->l('Wrong information'),
                    'desc' => $this->l('Yours information when is wrong data'),
                    'size' => '10',
                    'class' => 'rte',
                    'autoload_rte' => true,
                    'cols' => 60,
                    'name' => 'x13age_wrong_information',
                    'lang' => true
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Button confirmation'),
                    'size' => '5',
                    'name' => 'x13age_btn_confirmation',
                    'required' => false,
                    'lang' => true
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Button canceled'),
                    'size' => '5',
                    'name' => 'x13age_btn_canceled',
                    'required' => false,
                    'lang' => true
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Link to redirect user when have less than 18 old years'),
                    'size' => '5',
                    'name' => 'x13age_link',
                    'required' => false,
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save'),
                'class' => 'btn btn-default pull-right',
            ),
        );

        $fields_form[2]['form'] = array(
            'legend' => array(
                'title' => $this->l('Display'),
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('Width'),
                    'size' => '5',
                    'name' => 'x13age_width',
                    'required' => false,
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Height'),
                    'size' => '5',
                    'name' => 'x13age_height',
                    'required' => false,
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Round'),
                    'size' => '5',
                    'name' => 'x13age_round',
                    'required' => false,
                ),
                array(
                    'type' => 'color',
                    'label' => $this->l('Color frame'),
                    'size' => '5',
                    'name' => 'x13age_color_frame',
                    'required' => false,
                ),
                array(
                    'type' => 'color',
                    'label' => $this->l('Color button confirm'),
                    'size' => '5',
                    'name' => 'x13age_color_btn_confirm',
                    'required' => false,
                ),
                array(
                    'type' => 'color',
                    'label' => $this->l('Font color button confirm'),
                    'size' => '5',
                    'name' => 'x13age_color_font_btn_confirm',
                    'required' => false,
                ),
                array(
                    'type' => 'color',
                    'label' => $this->l('Color button canceled'),
                    'size' => '5',
                    'name' => 'x13age_color_btn_danger',
                    'required' => false,
                ),
                array(
                    'type' => 'color',
                    'label' => $this->l('Font color button canceled'),
                    'size' => '5',
                    'name' => 'x13age_color_font_btn_danger',
                    'required' => false,
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save'),
                'class' => 'btn btn-default pull-right',
            ),
        );
        $form = new HelperForm();
        $this->fields_form = array();
        $form->token = Tools::getAdminTokenLite('AdminModules');
        $form->submit_action = 'submitAddconfiguration';
        $form->tpl_vars = array(
            'fields_value' => array(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

//        $form->currentIndex = Tools::getHttpHost(true) . __PS_BASE_URI__ . basename(PS_ADMIN_DIR) . '/' . AdminController::$currentIndex . '&configure=' . $this->name . '&export=1';

        $lang = new Language((int) Configuration::get('PS_LANG_DEFAULT'));
        $form->default_form_language = $lang->id;

        $languages = Language::getLanguages(false);

        $form->tpl_vars['fields_value']['x13age_category'] = Tools::getValue('x13age_category', Configuration::get('x13age_category'));
        $form->tpl_vars['fields_value']['x13age_date'] = Tools::getValue('x13age_date', Configuration::get('x13age_date'));

        if ($this->ps_version >= '1.6') {
            $form->tpl_vars['fields_value']['x13age_categories_tree'] = Tools::getValue('x13age_categories_tree', (array) serialize(Configuration::get('x13age_categories_tree')));
        } else {
            $form->tpl_vars['fields_value']['x13age_categories_tree[]'] = Tools::getValue('x13age_categories_tree[]', (array) serialize(Configuration::get('x13age_categories_tree')));
        }

        foreach ($languages as $k => $lang) {
            $form->tpl_vars['fields_value']['x13age_message'][$lang['id_lang']] = Tools::getValue('x13age_message_' . $lang['id_lang'], Configuration::get('x13age_message_l' . $lang['id_lang']));
            $form->tpl_vars['fields_value']['x13age_title'][$lang['id_lang']] = Tools::getValue('x13age_title_' . $lang['id_lang'], Configuration::get('x13age_title_l' . $lang['id_lang']));
            $form->tpl_vars['fields_value']['x13age_wrong_information'][$lang['id_lang']] = Tools::getValue('x13age_wrong_information_' . $lang['id_lang'], Configuration::get('x13age_wrong_information_l' . $lang['id_lang']));

            $form->tpl_vars['fields_value']['x13age_btn_confirmation'][$lang['id_lang']] = Tools::getValue('x13age_btn_confirmation_' . $lang['id_lang'], Configuration::get('x13age_btn_confirmation_l' . $lang['id_lang']));
            $form->tpl_vars['fields_value']['x13age_btn_canceled'][$lang['id_lang']] = Tools::getValue('x13age_btn_canceled_' . $lang['id_lang'], Configuration::get('x13age_btn_canceled_l' . $lang['id_lang']));
        }

        $form->tpl_vars['fields_value']['x13age_link'] = Tools::getValue('x13age_link', Configuration::get('x13age_link'));


        $form->tpl_vars['fields_value']['x13age_width'] = Tools::getValue('x13age_width', Configuration::get('x13age_width'));
        $form->tpl_vars['fields_value']['x13age_height'] = Tools::getValue('x13age_height', Configuration::get('x13age_height'));
        $form->tpl_vars['fields_value']['x13age_round'] = Tools::getValue('x13age_round', Configuration::get('x13age_round'));

        $form->tpl_vars['fields_value']['x13age_color_frame'] = Tools::getValue('x13age_color_frame', Configuration::get('x13age_color_frame'));
        $form->tpl_vars['fields_value']['x13age_color_btn_confirm'] = Tools::getValue('x13age_color_btn_confirm', Configuration::get('x13age_color_btn_confirm'));
        $form->tpl_vars['fields_value']['x13age_color_font_btn_confirm'] = Tools::getValue('x13age_color_font_btn_confirm', Configuration::get('x13age_color_font_btn_confirm'));
        $form->tpl_vars['fields_value']['x13age_color_btn_danger'] = Tools::getValue('x13age_color_btn_danger', Configuration::get('x13age_color_btn_danger'));
        $form->tpl_vars['fields_value']['x13age_color_font_btn_danger'] = Tools::getValue('x13age_color_font_btn_danger', Configuration::get('x13age_color_font_btn_danger'));

        return $form->generateForm($fields_form);
    }

    public function getContent()
    {
        return $this->postProcess() . $this->renderForm();
    }

    public function postProcess()
    {
        if (Tools::isSubmit('submitAddconfiguration')) :
            Configuration::updateValue('x13age_category', Tools::getValue('x13age_category'));

            Configuration::updateValue('x13age_date', Tools::getValue('x13age_date'));

            if ((Tools::getValue('x13age_categories_tree'))) {
                Configuration::updateValue('x13age_categories_tree', serialize(Tools::getValue('x13age_categories_tree')));
                Configuration::updateValue('x13age_categories_tree_filled', serialize(Tools::getValue('x13age_categories_tree')));
            }
            // Content
            $languages = Language::getLanguages(false);

            foreach ($languages as $lang) {
                if ((Tools::getValue('x13age_message_' . $lang['id_lang']) && Tools::getValue('x13age_title_' . $lang['id_lang']) && Tools::getValue('x13age_wrong_information_' . $lang['id_lang']) && Tools::getValue('x13age_btn_confirmation_' . $lang['id_lang']) && Tools::getValue('x13age_btn_canceled_' . $lang['id_lang']) && Tools::getValue('x13age_link')
                        )) {
                    Configuration::updateValue('x13age_message_l' . $lang['id_lang'], Tools::getValue('x13age_message_' . $lang['id_lang']), true);
                    Configuration::updateValue('x13age_title_l' . $lang['id_lang'], Tools::getValue('x13age_title_' . $lang['id_lang']));

                    Configuration::updateValue('x13age_wrong_information_l' . $lang['id_lang'], Tools::getValue('x13age_wrong_information_' . $lang['id_lang']), true);

                    Configuration::updateValue('x13age_btn_confirmation_l' . $lang['id_lang'], Tools::getValue('x13age_btn_confirmation_' . $lang['id_lang']));
                    Configuration::updateValue('x13age_btn_canceled_l' . $lang['id_lang'], Tools::getValue('x13age_btn_canceled_' . $lang['id_lang']));

                    Configuration::updateValue('x13age_link', Tools::getValue('x13age_link'));
                }
            }

            if ((Tools::getValue('x13age_width')) && (Tools::getValue('x13age_height')) && (Tools::getValue('x13age_round'))) {
                Configuration::updateValue('x13age_width', Tools::getValue('x13age_width'));
                Configuration::updateValue('x13age_height', Tools::getValue('x13age_height'));
                Configuration::updateValue('x13age_round', Tools::getValue('x13age_round'));
            }

            if ((Tools::getValue('x13age_color_frame')) && (Tools::getValue('x13age_color_frame')) && (Tools::getValue('x13age_color_btn_confirm')) && (Tools::getValue('x13age_color_font_btn_confirm')) && (Tools::getValue('x13age_color_btn_danger')) && (Tools::getValue('x13age_color_font_btn_danger'))
            ) {
                Configuration::updateValue('x13age_color_frame', Tools::getValue('x13age_color_frame'));
                Configuration::updateValue('x13age_color_btn_confirm', Tools::getValue('x13age_color_btn_confirm'));
                Configuration::updateValue('x13age_color_font_btn_confirm', Tools::getValue('x13age_color_font_btn_confirm'));
                Configuration::updateValue('x13age_color_btn_danger', Tools::getValue('x13age_color_btn_danger'));
                Configuration::updateValue('x13age_color_font_btn_danger', Tools::getValue('x13age_color_font_btn_danger'));
            }

            return $this->displayConfirmation('Saved successfuly!');
        endif;
    }

    public function hookDisplayFooter()
    {
        $languages = Language::getLanguages(false);
        $message = array();
        $title = array();

        $message_fail = array();
        $btn_canceled = array();
        $btn_confirmation = array();

        $days_table = array();
        for ($i = 0; $i < 31; $i++) {
            array_push($days_table, ($i + 1));
        }

        $months_table = array();
        for ($i = 0; $i < 12; $i++) {
            array_push($months_table, ($i + 1));
        }
        $years_table = array();

        for ($i = date('Y'); $i > 1900; $i--) {
            array_push($years_table, ($i));
        }
        foreach ($languages as $lang) {
            $message[$lang['id_lang']] = Configuration::get('x13age_message_l' . $lang['id_lang']);
            $message_fail[$lang['id_lang']] = Configuration::get('x13age_wrong_information_l' . $lang['id_lang']);

            $title[$lang['id_lang']] = Configuration::get('x13age_title_l' . $lang['id_lang']);

            $btn_confirmation[$lang['id_lang']] = Configuration::get('x13age_btn_confirmation_l' . $lang['id_lang']);
            $btn_canceled[$lang['id_lang']] = Configuration::get('x13age_btn_canceled_l' . $lang['id_lang']);

            $message[$lang['id_lang']] = Configuration::get('x13age_message_l' . $lang['id_lang']);
            $message_fail[$lang['id_lang']] = Configuration::get('x13age_wrong_information_l' . $lang['id_lang']);
        }

        $this->smarty->assign(array(
            'x13age_link' => Configuration::get('x13age_link'),
            'x13age_date' => Configuration::get('x13age_date'),
            'x13age_category' => Configuration::get('x13age_category'),
            'x13age_days' => $days_table,
            'x13age_months' => $months_table,
            'x13age_years' => $years_table,
            'x13age_title' => $title[$this->context->language->id],
            'x13age_message' => $message[$this->context->language->id],
            'x13age_message_fail' => $message_fail[$this->context->language->id],
            'x13age_btn_confirmation' => $btn_confirmation[$this->context->language->id],
            'x13age_btn_canceled' => $btn_canceled[$this->context->language->id],
            'x13age_width' => !empty(Configuration::get('x13age_width')) ? Configuration::get('x13age_width') : '65%',
            'x13age_height' => !empty(Configuration::get('x13age_hight')) ? Configuration::get('x13age_hight') : 'auto',
            'x13age_round' => !empty(Configuration::get('x13age_round')) ? Configuration::get('x13age_round') : '0px',
            'x13age_c_frame' => !empty(Configuration::get('x13age_color_frame')) ? Configuration::get('x13age_color_frame') : '#505050',
            'x13age_c_btn_confirm' => !empty(Configuration::get('x13age_color_btn_confirm')) ? Configuration::get('x13age_color_btn_confirm') : '#11aa11',
            'x13age_c_font_btn_confirm' => !empty(Configuration::get('x13age_color_font_btn_confirm')) ? Configuration::get('x13age_color_font_btn_confirm') : '#ffffff',
            'x13age_c_btn_danger' => !empty(Configuration::get('x13age_color_btn_danger')) ? Configuration::get('x13age_color_btn_danger') : '#aa1111',
            'x13age_c_font_btn_danger' => !empty(Configuration::get('x13age_color_font_btn_danger')) ? Configuration::get('x13age_color_font_btn_danger') : '#ffffff',
            'x13age_uri' => __PS_BASE_URI__
        ));

        if (Configuration::get('x13age_category') == 0) {
            return $this->display(dirname(__FILE__), 'views/templates/front/hook/footer.tpl');
        } else {
            if (Context::getContext()->controller->php_self == 'product') {
                foreach (Product::getProductCategoriesFull(Tools::getValue('id_product')) as $category) {
                    if (in_array($category['id_category'], unserialize(Configuration::get('x13age_categories_tree_filled')))) {
                        return $this->display(dirname(__FILE__), 'views/templates/front/hook/footer.tpl');
                    }
                }
            }
            if (Context::getContext()->controller->php_self == 'category') {
                if (in_array(Tools::getValue('id_category'), (array) unserialize(Configuration::get('x13age_categories_tree_filled')))) {
                    return $this->display(dirname(__FILE__), 'views/templates/front/hook/footer.tpl');
                }
            }
        }
    }
}
