{*
 * Main tpl of module mjautocloseproduct
 * @author x13.pl.
 * @copyright (c) 2019, x13.pl
 * @license http://x13.pl x13.pl
 *}
 
<div class="age-confirmation">
    <div id="x13age" class="modal fade-in" style="width:{$x13age_width|escape:'htmlall':'UTF-8'};height:{$x13age_height|escape:'htmlall':'UTF-8'};margin: auto;" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">{l s='Auth' mod='x13age'}</h4>
                </div>
                <div class="modal-body">
                    <div class="text-center">
                        {if $x13age_date == '1'} 
                            <h6 class="text-center title">{$x13age_title|escape:'htmlall':'UTF-8'}</h6>
                            <div class="veryfication_info">
                                <div class='alert alert-danger' id="wrong_date" style="display:none;">{$x13age_message_fail|escape:'htmlall':'UTF-8' nofilter}</div>
                            </div>

                            <div class="form-group text-center row">
                                <div class="col-md-4">
                                    <label>{l s='Day' mod='x13age'}</label>
                                    <select name="day" class="form-control">
                                        {foreach $x13age_days as $day}
                                            <option value="{$day|escape:'htmlall':'UTF-8'}">{$day|escape:'htmlall':'UTF-8'}</option>
                                        {/foreach}
                                    </select>
                                </div>

                                <div class="col-md-4">
                                    <label>{l s='Month' mod='x13age'}</label>
                                    <select name="month" class="form-control">
                                        {foreach $x13age_months as $month}
                                            <option value="{$month|escape:'htmlall':'UTF-8'}">{$month|escape:'htmlall':'UTF-8'}</option>
                                        {/foreach}
                                    </select>
                                </div>

                                <div class="col-md-4">
                                    <label>{l s='Year' mod='x13age'}</label>
                                    <select name="year" class="form-control">
                                        {foreach $x13age_years as $year}
                                            <option value="{$year|escape:'htmlall':'UTF-8'}">{$year|escape:'htmlall':'UTF-8'}</option>
                                        {/foreach}
                                    </select>
                                </div>
                                    <input id="x13age_uri" type="hidden" value="{$x13age_uri|escape:'htmlall':'UTF-8'}" />
                            </div>
                        {else}
                            {$x13age_message|escape:'htmlall':'UTF-8' nofilter}
                        {/if}
                    </div>
                </div>
                <div class="modal-footer text-center">
                    {if $x13age_date == '1'}
                        <button type="button" class="btn btn-primary" style="background:{$x13age_c_btn_confirm|escape:'htmlall':'UTF-8'};color:{$x13age_c_font_btn_confirm|escape:'htmlall':'UTF-8'};" id="verifyAge">{$x13age_btn_confirmation|escape:'htmlall':'UTF-8'}</button>
                    {else}
                        <button type="button" id="addAgeCookie" class="btn btn-primary" data-dismiss="modal" style="background:{$x13age_c_btn_confirm|escape:'htmlall':'UTF-8'};color:{$x13age_c_font_btn_confirm|escape:'htmlall':'UTF-8'};">{$x13age_btn_confirmation|escape:'htmlall':'UTF-8'}</button>

                    {/if}
                    <a href="{$x13age_link|escape:'htmlall':'UTF-8'}" class="btn btn-secondary" style="background:{$x13age_c_btn_danger|escape:'htmlall':'UTF-8'};color:{$x13age_c_font_btn_danger|escape:'htmlall':'UTF-8'};">{$x13age_btn_canceled|escape:'htmlall':'UTF-8'}</a>
                </div>
            </div>  
        </div>
    </div>
</div>